import codecs
import os
import re


controller = "create_database.run"
is_file = "@runFile"
is_parse_and_run_file = "@parseAndRunFile"
is_var = "@variable"
charset = "utf-8"
new_line = "\n"

variables = {}

errors = []

##################################################################
# FUNCTIONS
##################################################################


def get_variable_name_prefix(var):
	return "variable(" + var


def get_variable(line):
	line = line[len(is_var) + 1:].replace(new_line, "")
	variables[line[:line.find(" ")]] = line[line.find(" ") + 1:]


def get_filename_by_separator(line):
	return (line[line.index(" ") + 1:]).replace(new_line, "")

def get_data_from_file(line):

	# get filename
	filename = get_filename_by_separator(line)

	try:

		return codecs.open(filename, 'r', charset).readlines()

	except:

		errors.append("\nFILE MIGHT BE MISSING\nline = " + line + "file = " + filename)
		print errors[len(errors) - 1]

		return []

def get_queries(file_query):
	queries = []

	for line in file_query:
		# skip comments and empty lines
		if line[0] == "#" or line[0] == new_line:
			continue

		# check if it is file
		elif is_file in line:
			queries.extend(get_data_from_file(line))

		# check if it's variable
		elif is_var in line:
			get_variable(line)

		# check if it's variable
		elif is_parse_and_run_file in line:
			queries.append(get_queries(open(get_filename_by_separator(line), 'r')))

		# else it's query
		else:
			queries.append(line)

	# remove empty element and new lines
	return [x for x in map(lambda s: s.strip(), filter(None, queries)) if x]

def create_mysql_queries():
	queries = new_line.join(get_queries(open(controller, 'r')))

	# set variables
	for key, value in variables.iteritems():
		# replace variables without parameter
		tmp = get_variable_name_prefix(key)
		queries = queries.replace(tmp + ')', value)
		# replace variables with parameter
		tmp = tmp.replace("(", "\(") + ',([^\)]*)\)'
		tmp = re.compile(tmp)
		it = re.finditer(tmp, queries)
		for match in it:
			m = match.group()
			queries = queries.replace(m, value.replace("?", re.findall(tmp, m)[0].strip()))

	# check if windows to copy queries to clipboard
	if os.name == 'nt':
		from Tkinter import Tk
		r = Tk()
		r.withdraw()
		r.clipboard_clear()
		r.clipboard_append(queries)
		r.update()  # now it stays on the clipboard after the window is closed
		r.destroy()

		print "#" * 50
		print "# COPIED TO CLIPBOARD"
		print "#" * 50

		if len(errors) > 0:
			print "\nTHERE ARE SOME ERRORS! PLEASE WAIT UNTIL SCRIPT FINISHED!\n"

		import time
		print "Sleeping for 5 seconds\n"
		time.sleep(5)

	return queries


#################################################################
# MAIN PART
#################################################################

print "Starting...\n\n"

print create_mysql_queries()

print "\n\nFinishing..."

if len(errors) > 0:

	print "#" * 50
	print "\n\n\n\nERRORS!!!!!"
	print "#" * 50

	for error in errors:
		print error
