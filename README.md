This project is created to join all queries from separated files to one query. There is a controller file which manages script and file sequence. This file is named "create_database.run". This file can hold 4 things.
1. When a line starts with "#" it's comment and it's skipped by the script
2. When a line starts with "@runFile filename.ext" the script reads all lines from file "filename.ext". There is no comment for the initial script in files metioned after "@runFile" command so all lines are readed.
3. When a line starts with "@parseAndRunFile filename.ext" the script reads all lines from file "filename.ext". Supports comments and variables.
4. When a line starts with "@variable variableName variableData" it declares variable with name "variableName" and data "variableData". Variables can be used with "variable(variableName)" so script will replace it with variableData. !IMPORTANT! Variable declaration must be in only ONE line!
5. Variables support only 1 parameter. Parameters are declared with ? in variableData. Usage: "variable(variableName, paramValue)" and "?" in variableData is replaced by paramValue.
6. In this file you could write queries too (just write them down).

!IMPORTANT! Only controller file could set ``@runFile`` and ``@variable`` and use comments (``#``). Other files can only use queries and variable(variableName).

If the file given after ``@runFile`` command is missing then script will report this error after finishing.

If host os is windows all queries are copied to clipboard automatically.
